package com.classpathio.orders.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.github.javafaker.Faker;

public class OrderTests {
	
	private Faker faker = new Faker();
	
	@Test
	void testConstructor() {
		Customer customer = Customer.builder().email("test@gmail.com").name("vinod").build();
		Order order = Order.builder()
				.totalOrderPrice(2000)
				.customer(customer)
				.orderDate(faker.date().past(10, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
				.build();
		Assertions.assertNotNull(order);
		Assertions.assertEquals(order.getTotalOrderPrice(), 2000);
		Assertions.assertNotNull(order.getCustomer());
		Assertions.assertEquals(order.getCustomer().getName(), "vinod");
	}
	
	@Test
	void setters() {
		Customer customer = Customer.builder().email("test@gmail.com").name("vinod").build();
		
		Order order = Order.builder().build();
		
		order.setCustomer(customer);
		order.setLineItems(new HashSet<>());
		order.setOrderDate(LocalDate.now());
		order.setTotalOrderPrice(4000);
		
		assertNotNull(order);
		assertEquals(order.getTotalOrderPrice(), 4000);
		assertNotNull(order.getCustomer());
		assertEquals(order.getCustomer().getName(), "vinod");
		assertNotNull(order.getLineItems());
		assertTrue(order.getLineItems().size() == 0);
		
	}

}
