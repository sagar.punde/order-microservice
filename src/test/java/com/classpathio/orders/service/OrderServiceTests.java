package com.classpathio.orders.service;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.classpathio.orders.model.Customer;
import com.classpathio.orders.model.Order;
import com.classpathio.orders.repository.OrderRepository;

/*
 * Template for testing
 * 
 * 1. Set the expectations on the Mock object
 * 2. Call the execute the method on the target
 * 3. Assert the expectations
 * 4. Verify that all the expectations are met on the mock object
 */
@ExtendWith(MockitoExtension.class)
public class OrderServiceTests {

	@Mock
	private OrderRepository orderRepository;

	@InjectMocks
	private OrderServiceImpl orderService;

	Customer customer;
	Order order;

	@BeforeEach
	void setup() {
		customer = Customer.builder().email("test@gmail.com").name("vinod").build();
		order = Order.builder().build();
		order.setCustomer(customer);
		order.setLineItems(new HashSet<>());
		order.setOrderDate(LocalDate.now());
		order.setTotalOrderPrice(4000);
	}

	@Test
	void testSave() {
		// setting up the expectations
		when(this.orderRepository.save(Mockito.any(Order.class))).thenReturn(order);
		// execute
		Order order = this.orderService.saveOrder(this.order);

		// Assertions
		assertNotNull(order);
		assertEquals(order.getTotalOrderPrice(), 4000);
		assertNotNull(order.getCustomer());
		assertEquals(order.getCustomer().getName(), "vinod");
		assertNotNull(order.getLineItems());
		assertTrue(order.getLineItems().size() == 0);

		// verification
		Mockito.verify(this.orderRepository, times(1)).save(order);
	}

	@Test
	void fetchOrderById() {
		// setting up the expectations

		when(this.orderRepository.findById(anyLong())).thenReturn(Optional.of(order));
		Order order = this.orderService.fetchOrderById(22);

		// Assertions
		assertNotNull(order);
		assertEquals(order.getTotalOrderPrice(), 4000);
		assertNotNull(order.getCustomer());
		assertEquals(order.getCustomer().getName(), "vinod");
		assertNotNull(order.getLineItems());
		assertTrue(order.getLineItems().size() == 0);

		// verification
		verify(this.orderRepository, times(1)).findById(22L);

	}

	@Test
	void fetchInvalidOrderById() {
		// setting up the expectations

		when(this.orderRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));
		try {
			this.orderService.fetchOrderById(22);
			fail("should throw IllegalArgumentException");
		} catch (Exception e) {
			assertNotNull(e);
			assertTrue(e instanceof IllegalArgumentException);
		}

		// verification
		verify(this.orderRepository, times(1)).findById(22L);
	}

	@Test
	void fetchInvalidOrderById2() {
		// setting up the expectations

		when(this.orderRepository.findById(anyLong())).thenReturn(Optional.ofNullable(null));

		assertThrows(IllegalArgumentException.class, () -> {
			this.orderService.fetchOrderById(22);
		});
		// verification
		verify(this.orderRepository, times(1)).findById(22L);
	}

	@Test
	void fetchValieOrderById() {
		// setting up the expectations

		when(this.orderRepository.findById(anyLong())).thenReturn(Optional.of(order));

		Assertions.assertDoesNotThrow(() -> {
			Order order = this.orderService.fetchOrderById(22);
			// Assertions
			assertNotNull(order);
			assertEquals(order.getTotalOrderPrice(), 4000);
			assertNotNull(order.getCustomer());
			assertEquals(order.getCustomer().getName(), "vinod");
			assertNotNull(order.getLineItems());
			assertTrue(order.getLineItems().size() == 0);
		});
		// verification
		verify(this.orderRepository, times(1)).findById(22L);
	}

	@Test
	void fetchDeleteOrderById() {
		// setting up the expectations

		doNothing().when(this.orderRepository).deleteById(anyLong());

		this.orderService.deleteOrderById(22L);

		// verification
		verify(this.orderRepository, times(1)).deleteById(22L);
	}

	@Test
	void testFetchAll() {
		List<Order> orders = new ArrayList<>();
		orders.add(order);

		Page<Order> pageResponse = new PageImpl<>(orders);

		// set the expectation
		// mock repository method
		when(orderRepository.findAll(PageRequest.of(0, 10))).thenReturn(pageResponse);

		// invoke service method
		Map<String, Object> result = orderService.fetchAllOrders(0, 10);

		// verify result
		assertEquals(1, result.get("pages"));
		assertEquals(1, result.get("records"));
		assertEquals(orders, result.get("data"));
		// verification
		verify(this.orderRepository, times(1)).findAll(PageRequest.of(0, 10));
	}
}
