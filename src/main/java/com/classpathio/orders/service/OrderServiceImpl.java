package com.classpathio.orders.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.classpathio.orders.model.LineItem;
import com.classpathio.orders.model.Order;
import com.classpathio.orders.model.OrderDto;
import com.classpathio.orders.repository.OrderRepository;

@Service
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	private OrderRepository orderRepository;

	@Override
	public Order saveOrder(Order order) {
		return this.orderRepository.save(order);
	}

	@Override
	public Map<String, Object> fetchAllOrders(int page, int size) {
		
		PageRequest pageRequest = PageRequest.of(page, size);
		
		Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
		
		long totalElements = pageResponse.getTotalElements();
		int totalPages = pageResponse.getTotalPages();
		int noOrRecords = pageResponse.getNumberOfElements();
		List<Order> content = pageResponse.getContent();
		
		
		Map<String, Object> responseMap = new LinkedHashMap<>();
		
		responseMap.put("pages", totalPages);
		responseMap.put("records", noOrRecords);
		responseMap.put("data", content);
		
		return responseMap;
	}

	@Override
	public Order fetchOrderById(long id) {
		return this.orderRepository
						.findById(id)
						.orElseThrow(() -> new IllegalArgumentException("invalid order id"));
	}

	@Override
	public void deleteOrderById(long id) {
		this.orderRepository.deleteById(id);

	}

	@Override
	public Order updateOrder(long orderId, Order updatedOrder) {
		/*
		 * first fetch the order by orderId.
		 * If the order is present, then update the order with the updated order data
		 * persist the changes to the db
		 */
		Order existingOrder = this.orderRepository
										.findById(orderId)
										.orElseThrow(() -> new EntityNotFoundException("order is not present"));
		existingOrder.setCustomer(updatedOrder.getCustomer());
		Set<LineItem> existingLineItems = existingOrder.getLineItems();
		Set<LineItem> updatedLineItems = updatedOrder.getLineItems();
		
		//remove any lineItems that are not present in the updateLineItems
		existingLineItems.removeIf(lineItem -> !updatedLineItems.contains(lineItem));
		
		//Add any new LineItems to the existing order
		updatedLineItems.stream()
			.filter(lineItem -> !existingLineItems.contains(lineItem))
			.forEach( lineItem -> {
				//set both sides of the relationship
				existingOrder.addLineItem(lineItem);
			});
		
		return this.orderRepository.save(existingOrder);
	}

}
