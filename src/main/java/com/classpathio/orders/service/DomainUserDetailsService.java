package com.classpathio.orders.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.classpathio.orders.model.DomainUserDetails;
import com.classpathio.orders.model.User;
import com.classpathio.orders.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DomainUserDetailsService implements UserDetailsService {

	private final UserRepository userRepository;
	/*
	 * The input will be the username and the output should be the UserDetails
	 */

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User domainUser = this.userRepository.findByName(username).orElseThrow(() -> new UsernameNotFoundException("bad credentials"));
		return new DomainUserDetails(domainUser);
	}

}
