package com.classpathio.orders.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoderUtil {
	public static void main(String args[]) {
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		String rawString = "welcome";
		
		String encodedString1 = encoder.encode(rawString);
		String encodedString2 = encoder.encode(rawString);
		String encodedString3 = encoder.encode(rawString);
		String encodedString4 = encoder.encode(rawString);
		
		System.out.println("Encoded string :: "+ encodedString1);
		System.out.println("Encoded string :: "+ encodedString2);
		System.out.println("Encoded string :: "+ encodedString3);
		System.out.println("Encoded string :: "+ encodedString4);
		
		System.out.println(encoder.matches(rawString, encodedString1));
	}

}
