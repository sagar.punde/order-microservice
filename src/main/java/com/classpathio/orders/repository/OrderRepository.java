package com.classpathio.orders.repository;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.classpathio.orders.model.Order;
import com.classpathio.orders.model.OrderDto;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{
	
	Page<OrderDto> findBy(PageRequest pageRequest);
	
	Set<Order> findByOrderDateBetween(LocalDate startDate, LocalDate endDate);
	
	//Page<Order> findByNameLike(String name, PageRequest pageRequest);
	
	Page<Order> findByTotalOrderPriceGreaterThan(double price, PageRequest pageRequest);

}
