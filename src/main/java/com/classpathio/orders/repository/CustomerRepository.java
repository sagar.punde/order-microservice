package com.classpathio.orders.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.classpathio.orders.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{

}
