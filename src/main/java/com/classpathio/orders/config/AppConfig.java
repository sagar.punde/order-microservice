package com.classpathio.orders.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
	
	@ConditionalOnProperty(prefix = "app", name = "loadUser", havingValue = "true")
	@Bean
	public User userBean() {
		return new User();
	}
	
	@ConditionalOnBean(name = "userBean")
	@Bean
	public User userBasedOnBean() {
		return new User();
	}
	
	@ConditionalOnMissingBean(name = "userBean")
	@Bean
	public User userBasedOnMissingBean() {
		return new User();
	}
	
	@ConditionalOnMissingClass(value = "x.y.z.Foo")
	@Bean
	public User userBasedOnMissingClass() {
		return new User();
	}
	
}


class User {
	
}
