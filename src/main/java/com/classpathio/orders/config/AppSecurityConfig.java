package com.classpathio.orders.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import com.classpathio.orders.service.DomainUserDetailsService;

import lombok.RequiredArgsConstructor;

/**
 * Authentication - Username/password match Authorization - has necessary
 * permissions
 * 
 *
 */
@Configuration
@RequiredArgsConstructor
public class AppSecurityConfig extends WebSecurityConfigurerAdapter{
	
	private final DomainUserDetailsService userDetailsService;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.
			authorizeRequests()
				.antMatchers("/actuator/**", "/h2-console/**", "/h2-console**")
					.permitAll()
				.antMatchers(HttpMethod.GET, "/api/**")
					.hasRole("USER")
				.antMatchers(HttpMethod.POST, "/api/**")
					.hasRole("ADMIN")
				.antMatchers(HttpMethod.PUT, "/api/**")
					.hasRole("ADMIN")
				.antMatchers(HttpMethod.DELETE, "/api/**")
					.hasRole("ADMIN")
				.and()
					.csrf().disable()
					.cors().disable()
					.headers().frameOptions().disable()
				.and()
					.formLogin()
				.and()
					.httpBasic();
		

	}
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws  Exception {
		auth.userDetailsService(this.userDetailsService).passwordEncoder(this.passwordEncoder());
	}
}
